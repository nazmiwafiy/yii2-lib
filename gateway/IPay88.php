<?php

namespace codetitan\gateway;

use Yii;
use yii\helpers\Json;
use yii\helpers\Html;

/**
 * @version 2.0.0
 */
class IPay88 extends BasePayment
{
    public $url;
    public $urlRequery;
    public $merchantKey;
    public $merchantCode;

    public static $inputOrder = [
        'MerchantCode',
        'RefNo',
        'Amount',
        'ProdDesc',
        'UserName',
        'UserEmail',
        'Currency',
        'Signature',
    ];

    public $inputs;

    function __construct($url, $urlRequery, $merchantKey, $merchantCode)
    {
        $this->url = $url;
        $this->urlRequery = $urlRequery;
        $this->merchantKey = $merchantKey;
        $this->merchantCode = $merchantCode;
    }

    public function make($urls, $transactionId, $prodDesc, $userName, $userEmail)
    {
        $this->wrap('MYR');
        $transaction = array_pop($this->transactions);

        $amount = $transaction['amount']['total'];
        $currency = $transaction['amount']['currency'];

        $signature = $this->signRequest($transactionId, $amount, $currency);

        $this->inputs = $this->load([
            'MerchantCode' => $this->merchantCode, 'RefNo' => $transactionId, 'Amount' => $amount, 'ProdDesc' => $prodDesc,
            'UserName' => $userName, 'UserEmail' => $userEmail, 'Currency' => $currency, 'Signature' => $signature,
            'ResponseURL' => $urls['response'], 'BackendURL' => $urls['backend']
        ]);

        return $this;
    }

    public function toHtml($label = 'Proceed to Payment', $options = [])
    {
        $html = [];
        $html[] = Html::beginForm($this->url, 'post', ['name' => 'ePayment']);
        foreach ($this->inputs as $name => $value) {
            $html[] = Html::hiddenInput($name, $value);
        }
        $options = array_merge(['name' => 'Submit'], $options);
        $html[] = Html::submitButton($label, $options);
        $html[] = Html::endForm();

        return implode('', $html);
    }

    public function requery($transactionId, $amount)
    {
        $attributes = ['MerchantCode' => $this->merchantCode, 'RefNo' => $transactionId, 'Amount' => $amount];

        $params = '';
        foreach ($attributes as $name => $value) {
            $params .= $name.'='.$value.'&';
        }
        rtrim($params, '&');
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->urlRequery.'?'.$params);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    public function load($attributes)
    {
        if (count($attributes) > 1) {
            $sorted = [];
            foreach (static::$inputOrder as $name) {
                if (isset($attributes[$name])) {
                    $sorted[$name] = $attributes[$name];
                }
            }
            $attributes = array_merge($sorted, $attributes);
        }

        $output = [];
        foreach ($attributes as $name => $value) {
            $output[$name] = $value;
        }

        return $output;
    }

    public function signRequest($transactionId, $amount, $currency) 
    {
        $amount = preg_replace('/[^0-9]/', '', $amount);
        return $this->sign($this->merchantKey.$this->merchantCode.$transactionId.$amount.$currency);
    }

    public function signResponse($paymentId, $refNo, $amount, $currency, $status) 
    {
        $amount = preg_replace('/[^0-9]/', '', $amount);
        return $this->sign($this->merchantKey.$this->merchantCode.$paymentId.$refNo.$amount.$currency.$status);
    }

    public function sign($payload)
    {
        return base64_encode(hex2bin(sha1($payload)));
    }
}