<?php

namespace codetitan\tree;

use Yii;
use yii\db\Query;

/**
 * @version 1.0.0
 *
 * Handles closure tree traversal (list/map)
 */
class Closure extends BaseClosure
{
    public $stack = [];

    /**
     * Return node model
     */
    public function getNode($id)
    {
        $query = new \yii\db\Query;
        $query->from('{{%'.$this->table['reference'].'}}')->where(['id' => $id]);
        return $query->createCommand()->queryOne();	
    }

    /**
     * List the tree
     * @return array
     */
    public function getList($startNode, $params = [])
    {
        // Append start node
        $model = $this->getNode($startNode);
        $model['depth'] = 0;
        $model['parent'] = null;

        $this->stack = [];
        $this->stack[] = $model;

        $this->listNode($startNode, 1, $params);

        return $this->stack;
    }

    private function listNode($node, $currDepth, $params = [])
    {
        $children = $this->queryDescendant($node, 1, $params);
        if (count($children)) { // Traverse recursively
            foreach ($children as $child) {
                // Append additional information to array
                $child['depth'] = $currDepth;
                $child['parent'] = $node;
            
                $this->stack[] = $child;
                $newDepth = $currDepth + 1;

                $this->listNode($child['id'], $newDepth++, $params);
            }
        } 
    }

    /**
     * Map the tree
     * @return array
     */
    public function getMap($startNode, $params = [])
    {
        $model = $this->getNode($startNode);

        // Encapsulate in array
        return [$this->mapNode($startNode, $model, $params)];
    }

    private function mapNode($node, $model = null, $params = [])
    {
        $children = $this->queryDescendant($node, 1, $params);
        if (count($children)) { // Traverse recursively
            $nodes = [];
            foreach ($children as $child) {
                $nodes[] = $this->mapNode($child['id'], $child, $params);
            }
    
            return ['id' => $node, 'data' => $model, 'nodes' => $nodes];
        } else {
            return ['id' => $node, 'data' => $model];
        }
    }
}