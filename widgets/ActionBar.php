<?php

namespace codetitan\widgets;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * @copyright Copyright (c) 2016 Code Titan
 * @author David Cheang <david.cheang@codetitan.com.my>
 * @version 2.0.8
 *
 * Renders the navigation bar with actions
 */
class ActionBar extends \yii\bootstrap\Widget
{
    const PLACEMENT_LEFT = 'left';
    const PLACEMENT_RIGHT = 'right';

    public static $attributeOrder = [
        'label',
        'type',
        'size',
        'iconPlacement',
        'iconMap',
        'iconColor',
        'icon',
        'style',
        'onclick',
    ];

    public static $defaultAttribute = [
        'type' => 'default',
        'size' => 'xs',
        'iconPlacement' => self::PLACEMENT_LEFT,
        'iconMap' => 'glyphicon',
    ];

    public static $defaultRequired = [
        'edit', 
        'publish', 
        'unpublish', 
        'block', 
        'unblock', 
        'approve', 
        'reject', 
        'enable', 
        'disable', 
        'featured', 
        'delete',
    ];

    public static $defaultConfirm = [
        'publish', 
        'unpublish', 
        'block', 
        'unblock', 
        'approve', 
        'reject', 
        'enable', 
        'disable', 
        'featured', 
        'delete',
    ];

    private $params = [];

    public $target = null;
    public $template = null;
    public $buttons = [];
    public $buttonOptions = [];
    public $permissions = [];

    public $enableHiddenSelection = true;
    public $hideDisabled = false;

    public function registerTranslations()
    {
        $i18n = Yii::$app->i18n;
        $i18n->translations['widgets/actionbar'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@vendor/codetitan/widgets/messages',
            'fileMap' => [
                'widgets/actionbar' => 'actionbar.php',
            ],
        ];
    }

    public static function t($message, $params = [], $language = null)
    {
        return Yii::t('widgets/actionbar', $message, $params, $language);
    }

    public function init()
    {
        $this->registerTranslations();

        ActionBarAsset::register($this->getView(), \yii\web\View::POS_END);
    }

    public function run()
    {
        $html = [];
        $btn = [];
        $confirm = [];
        $required = [];

        // Prepare buttons based on template blueprint
        $regex = '/{(.*?)}/';
        preg_match_all($regex, $this->template, $matches);
        foreach ($matches[1] as $button) {
            $btn[$button] = $this->getDefaultButton($button);
        }

        // Implement button options (use default if not specified)
        if (isset($this->buttonOptions)) {
            if (!empty($this->buttonOptions['required'])) {
                foreach ($this->buttonOptions['required'] as $key) {
                    if (array_key_exists($key, $btn)) { // Button must exist
                        if (!in_array($key, $required)) $required[] = $key;
                    }
                }
            } else $required = static::$defaultRequired;

            if (!empty($this->buttonOptions['confirm'])) {
                foreach ($this->buttonOptions['confirm'] as $key) {
                    if (array_key_exists($key, $btn)) { // Button must exist
                        if (!in_array($key, $confirm)) $confirm[] = $key;
                    }
                }
            } else $confirm = static::$defaultConfirm;
        }

        // Overwrite buttons (if applicable)
        foreach ($this->buttons as $key => $button) 
        {
            if (is_array($button)) { // Customize via button builder
                $btn[$key] = $this->makeButton($key, $button);

                // Overwrite required and confirm from button parameters
                if (isset($button['required'])) {
                    if ($button['required']) {
                        if (!in_array($key, $required)) $required[] = $key;
                    } else {
                        if (($key = array_search($key, $required)) !== false) {
                            unset($required[$key]);
                        }
                    }
                }

                if (isset($button['confirm'])) {
                    if ($button['confirm']) {
                        if (!in_array($key, $confirm)) $confirm[] = $key;
                    } else {
                        if (($key = array_search($key, $confirm)) !== false) {
                            unset($confirm[$key]);
                        }
                    }
                }
            } elseif (is_callable($button) && ($button instanceof \Closure)) {
                $btn[$key] = call_user_func($button);
            }
        }

        // Implement permissions
        $js = [];
        foreach ($this->permissions as $action => $permission) {
            if (!$this->hasPermission($permission)) {
                if (!$this->hideDisabled) $js[] = "$('#action_".$action."').attr('disabled', true);";
                else unset($btn[$action]);
            }
        }
        if ($js) $this->getView()->registerJs(implode('', $js), yii\web\View::POS_END, 'action');

        // Map and replace template with actual buttons
        $template = preg_replace_callback($regex, function($match) use ($btn) {
            return $btn[$match[1]]; 
        }, $this->template);

        // Display action nav-bar
        $html[] = '<style type="text/css">.wrap {padding-top:35px;}</style>';
        $html[] = '<div class="actionbar" style="margin-top:-10px">';
//        $html[] = '<div class="container">';
        if ($btn) $html[] = '<div class="actionbtn">'.$template.'</div>';
        if ($this->enableHiddenSelection) {
            $html[] = Html::hiddenInput('actionbar_selection', '', ['id' => 'actionbar_selection']);
        }
//        $html[] = '</div>';
        $html[] = '</div>';

        $this->registerJs($required, $confirm);

        return implode('', $html);
    }

    protected function registerJs($required, $confirm)
    {
        $required = array_map(function($key) { return 'action_'.$key; }, $required);
        $confirm = array_map(function($key) { return 'action_'.$key; }, $confirm);

        $this->getView()->registerJs("
            var keys;
            var actionButton;
            $('button[type=\"submit\"]').click(function(e) {
                actionButton = $(this).attr('id');
            });

            var requiredList = ['".implode("', '", $required)."'];
            var confirmList = ['".implode("', '", $confirm)."'];

            var isConfirmed = false;
            $('form').on('beforeSubmit.yii', function(e) {
                var form = $(this);
            
                // Remove redundant auto-generated hidden input
                if ($('form input:hidden[name^=\'action[\']').length > 1) {
                    $('form input:hidden[name^=\'action[\']:first').remove();
                }

                if ($('#".$this->target."').length) {
                    keys = $('#".$this->target."').yiiGridView('getSelectedRows');

                    if ($('#actionbar_selection').length) {
                        $('#actionbar_selection').val(JSON.stringify(keys));
                    }
                }

                if ($.inArray(actionButton, requiredList) >= 0) {
                    if ($('#".$this->target."').length && (keys.length <= 0)) {
                        alert('".static::t('No item selected')."');
                        return false;
                    } else if (actionButton == 'action_delete') {
                        if (!confirm('".static::t('Are you sure you want to delete this item')."?')) return false;
                        else isConfirmed = true;
                    }
                }

                if ($.inArray(actionButton, confirmList) >= 0) {
                    if (!isConfirmed) {
                        if (!confirm('".static::t('Are you sure you want to perform this action')."?')) return false;
                    }
                }
            });

            function gotoIndex() {
                location.href = '".Url::to(['/'.dirname(Yii::$app->controller->route)])."';
            }
        ", yii\web\View::POS_END, 'action-selection');
    }

    protected function getDefaultButton($key)
    {
        $btn = null;
        switch($key) 
        {
            case 'new':         $params = ['label' => static::t('New'),             'icon' => 'plus-sign',      'type' => 'success', 'style' => 'width:100px']; break;
            case 'edit':        $params = ['label' => static::t('Edit'),            'icon' => 'edit']; break;

            case 'publish':     $params = ['label' => static::t('Publish'),         'icon' => 'ok-sign',        'iconColor' => 'green']; break;
            case 'unpublish':   $params = ['label' => static::t('Unpublish'),       'icon' => 'remove-sign',    'iconColor' => 'red']; break;
            case 'block':       $params = ['label' => static::t('Block'),           'icon' => 'ban-circle',     'iconColor' => 'red']; break;
            case 'unblock':     $params = ['label' => static::t('Unblock'),         'icon' => 'share-alt']; break;
            case 'approve':     $params = ['label' => static::t('Approve'),         'icon' => 'ok',             'iconColor' => 'green']; break;
            case 'reject':      $params = ['label' => static::t('Reject'),          'icon' => 'remove',         'iconColor' => 'red']; break;
            case 'enable':      $params = ['label' => static::t('Enable'),          'icon' => 'ok-sign',        'iconColor' => 'green']; break;
            case 'disable':     $params = ['label' => static::t('Disable'),         'icon' => 'remove-sign',    'iconColor' => 'red']; break;

            case 'featured':    $params = ['label' => static::t('Featured'),        'icon' => 'star',           'iconColor' => 'yellow']; break;
            case 'save':        $params = ['label' => static::t('Save'),            'icon' => 'floppy-saved',   'type' => 'success', 'style' => 'width:100px']; break;
            case 'save2close':  $params = ['label' => static::t('Save & Close'),    'icon' => 'ok',             'iconColor' => 'green']; break;
            case 'save2new':    $params = ['label' => static::t('Save & New'),      'icon' => 'plus',           'iconColor' => 'green']; break;
            case 'save2copy':   $params = ['label' => static::t('Save as Copy'),    'icon' => 'file',           'iconColor' => 'green']; break;

            case 'cancel':      $params = ['label' => static::t('Cancel'),          'icon' => 'floppy-remove',  'iconColor' => 'red',   'url' => ['index']]; break;
            case 'close':       $params = ['label' => static::t('Close'),           'icon' => 'remove',         'iconColor' => 'red',   'url' => ['index']]; break;
            case 'back':        $params = ['label' => static::t('Back'),            'icon' => 'chevron-left']; break;
            case 'jsback':      $params = ['label' => static::t('Back'),            'icon' => 'chevron-left',   'onclick' => 'window.history.back(); return false']; break;
            case 'delete':      $params = ['label' => Yii::t('yii', 'Delete'),      'icon' => 'trash']; break;

            case '|':           $btn = ' | '; break; // Separator
            default: break;
        }
    
        if (isset($params)) $btn = $this->makeButton($key, $params);
        return $btn;
    }

    protected function makeButton($key, $options = [])
    {
        $this->params = array_merge(static::$defaultAttribute, static::loadAttributes($options));

        // Default parameters
        if (!isset($this->params['label'])) $this->params['label'] = ucwords($key);
        if (isset($this->params['iconColor'])) $this->params['icon'] .= ' color-'.$this->params['iconColor'];

        if (isset($this->params['icon'])) {
            $iconClass = $this->params['iconMap'].' '.$this->params['iconMap'].'-'.$this->params['icon'];
            $this->params['icon'] = '<i class="'.$iconClass.'"></i>';
        } else $this->params['icon'] = null;

        if ($this->params['iconPlacement'] == self::PLACEMENT_LEFT) $label = $this->params['icon'].' '.$this->params['label'];
        else $label = $this->params['label'].' '.$this->params['icon'];

        // Generate button options
        $options = [
            'id' => 'action_'.$key, 
            'name' => 'action['.$key.']', 
            'class' => 'btn btn-'.$this->params['type'].' btn-'.$this->params['size'],
            'title' => $this->params['label'],
            'aria-label' => $this->params['label'],
            'data-pjax' => '0',
            'value' => 1,
        ];

        if (isset($this->params['style'])) $options['style'] = $this->params['style'];
        if (isset($this->params['onclick'])) $options['onclick'] = $this->params['onclick'];

        if (isset($this->params['url'])) {
            if (isset($this->permissions[$key]) && !$this->hasPermission($this->permissions[$key])) {
                $this->params['url'] = 'javascript:void(0)';
            }
            if (isset($this->params['target'])) $options['target'] = $this->params['target'];
            return Html::a($label, $this->params['url'], $options);
        } else return Html::submitButton($label, $options);
    }

    protected function hasPermission($permission)
    {
        if (is_bool($permission) || is_integer($permission)) {
            if ($permission) return true;
            else return false;
        }

        return Yii::$app->user->can($permission);
    }

    /**
     * Load attributes to array.
     */
    private static function loadAttributes($attributes)
    {
        if (count($attributes) > 1) {
            $sorted = [];
            foreach (static::$attributeOrder as $name) {
                if (isset($attributes[$name])) {
                    $sorted[$name] = $attributes[$name];
                }
            }
            $attributes = array_merge($sorted, $attributes);
        }

        $output = [];
        foreach ($attributes as $name => $value) {
            $output[$name] = $value;
        }

        return $output;
    }
}