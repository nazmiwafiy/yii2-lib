<?php

namespace codetitan\widgets;

use Yii;
use yii\helpers\Html;

/**
 * @copyright Copyright (c) 2016 Code Titan
 * @author David Cheang <david.cheang@codetitan.com.my>
 * @version 2.0.2
 *
 * @dependencies: kartik-v/yii2-icons
 * Renders the page navigations for GridView.
 */
class GridNav extends \yii\bootstrap\Widget
{
    public $dataProvider;
    public $output;

    private $currPage;
    private $lastPage;

    private $pager, $total, $perPage;

    public function registerTranslations()
    {
        $i18n = Yii::$app->i18n;
        $i18n->translations['widgets/gridnav'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@vendor/codetitan/widgets/messages',
            'fileMap' => [
                'widgets/gridnav' => 'gridnav.php',
            ],
        ];
    }

    public static function t($message, $params = [], $language = null)
    {
        return Yii::t('widgets/gridnav', $message, $params, $language);
    }

    public function init()
    {
        $this->registerTranslations();

        GridNavAsset::register($this->getView(), \yii\web\View::POS_END);

        $options = [5, 10, 15, 20, 25, 30, 50];
        $option['page-limit'] = array_combine($options, $options);

        // Add custom page limit (if applicable)
        $perPage = Yii::$app->request->get('per-page', '20');
        if (!in_array($perPage, $options) && (intval($perPage) > 0)) {
            $option['page-limit'][$perPage] = $perPage;
            asort($option['page-limit']);
        }

        $this->pager = $this->makePager();
        $this->total ='['.static::t('Total <b>{total}</b> records', ['total' => $this->dataProvider->getTotalCount()]).']';
        $this->perPage = Html::dropDownList('per-page', $perPage, $option['page-limit'], [
            'id' => 'per-page', 'class' => 'form-control',' style' => 'width:60px;height:24px'
        ]);

        $this->getView()->registerJs("
            $('#per-page').change(function() {
                location.href = updateGetParam($(location).attr('href'), 'per-page', $('#per-page').val());
            });

            $(document).on('keydown', '.nav-page', function(e) {
                if(e.which === 13) {
                    var page = $(this).val().replace(/[^0-9]/g, '');
                    location.href = updateGetParam($(location).attr('href'), 'page', page);
                }
            });
        ", yii\web\View::POS_END, 'navigation');

        $html = $this->makeNav();
        $html .= '<div class="gridnav-container">'.$this->output.'</div>';
        $html .= $this->makeNav(false);

        echo $html;
    }

    private function makeNav($showPerPage = true)
    {
        $html = '<div class="gridnav">';
        $html .= '<div class="pull-left">'.$this->pager.'</div>';
        $html .= '<div class="pull-left" style="margin-left:10px;">'.$this->total.'</div>';

        if ($showPerPage && ($this->dataProvider->getPagination() !== false)) {
            $html .= '<div class="pull-right">'.$this->perPage.'</div>';
            $html .= '<div class="clearfix"></div>';
        }
        $html .= '</div>';

        return $html;
    }

    private function makePager() 
    {
        $list = [];
        $pagination = $this->dataProvider->getPagination();

        if ($pagination === false) {
            $this->currPage = 0;
            $this->lastPage = 1;
        } else {
            $this->currPage = $pagination->getPage();
            $this->lastPage = $pagination->getPageCount();
        }

        $input = Html::textInput('page', $this->currPage + 1, [
            'class' => 'pull-left nav-page',
        ]);

        $list[] = $this->makePagerLink($pagination, 'first');
        $list[] = $this->makePagerLink($pagination, 'prev');
        $list[] = '<li>'.$input.'</li>';
        $list[] = $this->makePagerLink($pagination, 'next');
        $list[] = $this->makePagerLink($pagination, 'last');

        $pager = '<ul class="pagination" style="margin:0;">'.implode('', $list).'</ul>';
    
        $html = '<div class="pull-left">'.static::t('Page').'</div>';
        $html .= '<div class="pull-left" style="margin:0 5px;">'.$pager.'</div>';
        $html .= '<div class="pull-left">'.static::t('of {total} pages', ['total' => $this->lastPage]).'</div>';
        $html .= '<div class="clearfix"></div>';
    
        return $html;
    }


    private function makePagerLink($pagination, $class)
    {
        if ($pagination !== false) $links = $pagination->getLinks();

        switch ($class) {
            default:
            case 'first':   $icon = 'angle-double-left';    $page = 0;                      break;
            case 'prev':    $icon = 'angle-left';           $page = $this->currPage - 1;    break;
            case 'next':    $icon = 'angle-right';          $page = $this->currPage + 1;    break;
            case 'last':    $icon = 'angle-double-right';   $page = $this->lastPage - 1;    break;
        }

        if (isset($links[$class])) {
            return '<li class="'.$class.'">'.Html::a('<i class="fa fa-'.$icon.'"></i>', $links[$class], ['data-page' => $page]).'</li>';
        } else {
            return '<li class="'.$class.' disabled"><span><i class="fa fa-'.$icon.'"></i></span></li>';
        }
    }
}