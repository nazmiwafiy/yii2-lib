<?php

namespace codetitan\handlers;

use Yii;

/**
 * @version 2.0.0
 */
class DependentHandler
{
    /**
     * Validate reference model
     * @return mixed
     */
    public static function fetch($modelClass)
    {
        $data = static::getSession();
        if (isset($data[$modelClass])) {
            $arr = $data[$modelClass];
            $model = $arr['model']::find()->where($arr['condition'])->one();

            if ($model) return $model;
            Yii::$app->controller->redirect($arr['returnUrl']);
        }

        Yii::$app->controller->goBack();
        Yii::$app->end();
    }

    public static function push($modelClass, $parentModelPath, $condition, $returnUrl, $params = []) 
    {
        $data = static::getSession();
        if (!$data) $data = [];

        $arr['model'] = $parentModelPath;
        $arr['condition'] = $condition;
        $arr['returnUrl'] = $returnUrl;
        if ($params) $arr['params'] = $params;

        $data[$modelClass] = $arr;
        static::setSession($data);
    }

    public static function pop($modelClass) 
    {
        $data = [];
        $folder = self::getSession();
        if (isset($folder[$modelClass])) {
            $data = $folder[$modelClass];
            unset($folder[$modelClass]);
            self::setSession($folder);
        }

        return $data;
    }

    private static function getSession($modelClass = null)
    {
        if ($modelClass && isset(Yii::$app->session['dependent'][$modelClass])) {
            $data = Yii::$app->session['dependent'][$modelClass];
            static::setSession($data); // Refresh session

            return $data;
        }

        return Yii::$app->session['dependent'];
    }

    private static function setSession($data)
    {
        Yii::$app->session['dependent'] = $data;
    }

    public static function destroySession()
    {
        unset(Yii::$app->session['dependent']);
    }
}