<?php

namespace codetitan\helpers;

use Yii;
use yii\helpers\Html;

use kartik\widgets\DatePicker;

/**
 * @version 2.0.1
 */
class FilterRangeHelper
{
    /**
     * Renders range as text input.
     * Include class 'filter-right' to right align text
     */
    public static function textInput($model, $attrFrom, $attrTo, $options = [])
    {
        $placeholders = self::resolvePlaceholder($options);

        $from = Html::activeTextInput($model, $attrFrom, ['class' => 'form-control', 'placeholder' => $placeholders['from']]);
        $to = Html::activeTextInput($model, $attrTo, ['class' => 'form-control', 'placeholder' => $placeholders['to']]);

        return self::render($from, $to, $options);
    }

    /**
     * Renders range as date picker.
     * Requires \kartik\widgets\DatePicker
     */
    public static function datePicker($model, $attrFrom, $attrTo, $options = [])
    {
        $placeholders = self::resolvePlaceholder($options);

        $from = DatePicker::widget([
            'model' => $model,
            'attribute' => $attrFrom,
            'type' => DatePicker::TYPE_INPUT,
            'options' => ['placeholder' => $placeholders['from']],
            'pluginOptions' => [
                'format' => (isset($options['itemOptions']['format']))?$options['itemOptions']['format']:'yyyy-mm-dd',
                'autoclose' => true,
            ],
        ]);

        $to = DatePicker::widget([
            'model' => $model,
            'attribute' => $attrTo,
            'type' => DatePicker::TYPE_INPUT,
            'options' => ['placeholder' => $placeholders['to']],
            'pluginOptions' => [
                'format' => (isset($options['itemOptions']['format']))?$options['itemOptions']['format']:'yyyy-mm-dd',
                'autoclose' => true,
            ],
        ]);

        return self::render($from, $to, $options);
    }

    private static function resolvePlaceholder($options)
    {
        $placeholders = ['from' => 'From', 'to' => 'To'];
        if (isset($options['itemOptions']['placeholder'])) {
            list($placeholders['from'], $placeholders['to']) = $options['itemOptions']['placeholder'];
        }

        return $placeholders;
    }

    /**
     * Renders the range inputs.
     */
    public static function render($inputFrom, $inputTo, $options = [])
    {
        if (isset($options['itemOptions'])) unset($options['itemOptions']);

        $class = [];
        $class[] = 'filter-range';
        if (isset($options['class'])) {
            $arr = explode(' ', $options['class']);
            $class = array_merge($class, $arr);
        }

        $content = '';
        $content .= Html::beginTag('div', ['class' => $class]);
        $content .= Html::tag('div', $inputFrom);
        $content .= Html::tag('div', $inputTo);
        $content .= Html::endTag('div');

        return $content;
    }
}