<?php

namespace codetitan\widgets;

use yii\web\AssetBundle;

/**
 * @copyright Copyright (c) 2016 Code Titan
 * @author David Cheang <david.cheang@codetitan.com.my>
 * @version 2.0.0
 */
class GridNavAsset extends AssetBundle
{
    public $sourcePath = '@vendor/codetitan/yii2-lib/widgets/assets';
    public $css = [
        'css/gridnav.css',
    ];
    public $js = [
        'js/param.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
