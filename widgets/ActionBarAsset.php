<?php

namespace codetitan\widgets;

use yii\web\AssetBundle;

/**
 * @copyright Copyright (c) 2016 Code Titan
 * @author David Cheang <david.cheang@codetitan.com.my>
 * @version 2.0.0
 */
class ActionBarAsset extends AssetBundle
{
    public $sourcePath = '@vendor/codetitan/yii2-lib/widgets/assets';
    public $css = [
        'css/actionbar.css',
        'css/color.css',
    ];
    public $js = [];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
