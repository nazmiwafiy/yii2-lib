<?php

namespace codetitan\helpers;


use Yii;

/**
 * @version 1.1.0
 */
class Html extends \yii\helpers\BaseHtml
{
    /**
     * Renders the generate password input.
     */
    public static function generatePasswordInput($name, $value = null, $options = [])
    {
        Yii::$app->view->registerJs("
            function generatePassword(inputId, length) {
                var charset = 'abcdefghjknpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789';
                var password = '';

                for (var i=0, n=charset.length; i<length; ++i) {
                    password += charset.charAt(Math.floor(Math.random() * n));
                }

                $('#' + inputId).val(password);
                $('#' + inputId).focus();
            }
        ", \yii\web\View::POS_END, 'generate-password-input');

        $btn = static::button('Generate Password', [
            'class' => 'btn btn-default',
            'onclick' => 'generatePassword("'.$options['id'].'", 16)',
        ]);

        $options = array_merge(['class' => 'form-control'], $options);
        $input = static::input('text', $name, $value, $options);
        $input .= static::tag('span', $btn, ['class' => 'input-group-btn']);

        $html = static::tag('div', $input, ['class' => 'input-group']);
        return $html;
    }

    /**
     * Renders the file input.
     */
    public static function fileInput($name, $value = null, $options = [], $clientOptions = [])
    {
        $emptyText = '<i>Please upload a file.</i>';
        static::registerFileInputAsset($emptyText);

        $btnReset = static::button('<i class="glyphicon glyphicon-remove" style="color:#666"></i>', [
            'class' => 'btn btn-default',
            'onclick' => 'resetFileInput("'.$options['id'].'")',
        ]);

        if ($preview = static::renderFilePreview($value, (isset($clientOptions['filename']))?$clientOptions['filename']:null)) {
            $btnRemove = static::button('Remove File', [
                'class' => 'btn btn-danger',
                'onclick' => 'removeFile("'.$options['id'].'")',
            ]);
        } else {
            $preview = $emptyText;
            $btnRemove = static::button('Remove File', [
                'class' => 'btn btn-danger disabled',
            ]);
        }

        $input = static::input('file', $name, $value, $options);
        $input .= static::tag('span', $btnReset.$btnRemove, ['class' => 'input-group-btn']);

        $html = static::tag('div', $preview);
        $html .= static::tag('div', $input, ['class' => 'input-group']);
        return static::tag('div', $html, ['class' => 'file-input', 'id' => 'file-input-'.$options['id']]);
    }

    /**
     * Renders the image input.
     */
    public static function imageInput($name, $value = null, $options = [])
    {
        $emptyText = '<i>Image preview not available.</i>';
        static::registerFileInputAsset($emptyText);

        $btnReset = static::button('<i class="glyphicon glyphicon-remove" style="color:#666"></i>', [
            'class' => 'btn btn-default',
            'onclick' => 'resetFileInput("'.$options['id'].'")',
        ]);

        if ($preview = static::renderImagePreview($value)) {
            $btnRemove = static::button('Remove Image', [
                'class' => 'btn btn-danger',
                'onclick' => 'removeFile("'.$options['id'].'")',
            ]);
        } else {
            $preview = $emptyText;
            $btnRemove = static::button('Remove Image', [
                'class' => 'btn btn-danger disabled',
            ]);
        }

        $input = static::input('file', $name, $value, $options);
        /*close*/$input .= static::tag('span', $btnReset.$btnRemove, ['class' => 'input-group-btn']);

        /*close*/$html = static::tag('div', $preview, ['class' => 'text-center']);
        $html .= static::tag('div', $input, ['class' => 'input-group']);

        return static::tag('div', $html, ['class' => 'file-input', 'id' => 'file-input-'.$options['id']]);
    }

    public static function registerFileInputAsset($emptyText)
    {
        Yii::$app->view->registerCss("
            .file-input > div:first-child {
                background-color:#f8f9f9;
                padding:3px 10px;
                border:1px solid #ccc;border-bottom:none;
                border-top-left-radius:5px;border-top-right-radius:5px;
            }
            .file-input .input-group {width:100%;}
            .file-input .input-group input {
                width:100%;height:34px;padding-top:8px;margin-right:0px;
                border:1px solid #ccc;border-bottom-left-radius:5px;border-bottom-right-radius:0;
            }
            .file-input .input-group-btn button {
                border-top-right-radius:0;margin-right:-1px;
            }
            @-moz-document url-prefix() { 
              .file-input .input-group input {padding-top:0;}
            }
        ");

        Yii::$app->view->registerJs("
            function resetFileInput(inputId) {
                $('.file-input input[id=' + inputId + ']').val('');
            }

            function removeFilePreview(inputId) {
                $('#file-input-' + inputId + ' div:first').html('".$emptyText."');
                $('#file-input-' + inputId + ' .input-group-btn .btn-danger').attr('disabled', true);
            }

            function removeFile(inputId) {
                if (confirm('".Yii::t('yii', 'Are you sure you want to delete this item?')."')) {
                    $.post(window.location.href, {'actionRemoveFile':inputId}).done(function(data) {
                        if (data) removeFilePreview(inputId);
                    });
                }
            }
        ", \yii\web\View::POS_END, 'file-input');
    }

    /**
     * Renders the active generate password input.
     */
    public static function activeGeneratePasswordInput($model, $attribute, $options = [])
    {
        return static::getInput('generatePasswordInput', $model, $attribute, $options);
    }

    /**
     * Renders the active image input.
     */
    public static function activeImageInput($model, $attribute, $options = [])
    {
        return static::getInput('imageInput', $model, $attribute, $options);
    }

    /**
     * Renders the active file input.
     */
    public static function activeFileInput($model, $attribute, $options = [], $clientOptions = [])
    {
        return static::getInput('fileInput', $model, $attribute, $options, $clientOptions);
    }

    /**
     * Renders the input based on data provided.
     */
    private static function getInput($input, $model, $attribute, $options = [], $clientOptions = [])
    {
        $name = isset($options['name']) ? $options['name'] : static::getInputName($model, $attribute);
        $value = isset($options['value']) ? $options['value'] : static::getAttributeValue($model, $attribute);
        if (!array_key_exists('id', $options)) {
            $options['id'] = static::getInputId($model, $attribute);
        }
        return static::$input($name, $value, $options, $clientOptions);
    }

    /**
     * Renders a downloadable link.
     * @param string $src the path to the image file.
     * @return link, or false if file does not exist.
     */
    private static function renderFilePreview($src, $filename = null)
    {
        $path = pathinfo(Yii::getAlias($src));

        if (file_exists($src)) {
            $url = Yii::$app->urlManager->baseUrl.'/'.$src;
            if (!$filename) $filename = basename($url);
            return static::a($filename, $url, ['target' => '_blank']);
        }
        return false;
    }

    /**
     * Renders a preview of the image.
     * Output by image input.
     * @param string $src the path to the image file.
     * @return image, or false if image file does not exist.
     */
    private static function renderImagePreview($src)
    {
        $path = pathinfo(Yii::getAlias($src));

        if (file_exists($src)) {
            $url = Yii::$app->urlManager->baseUrl.'/'.$src;
            return static::Img($url, ['style' => 'max-height:150px']);
        }
        return false;
    }
}