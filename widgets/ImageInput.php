<?php

namespace codetitan\widgets;

use Yii;
use codetitan\helpers\Html;

/**
 * @copyright Copyright (c) 2016 Code Titan
 * @author David Cheang <david.cheang@codetitan.com.my>
 * @version 1.0.0
 */
class ImageInput extends \yii\widgets\InputWidget
{
    /**
     * @inheritdoc
     */
    public function run()
    {
        parent::run();

        echo Html::activeImageInput($this->model, $this->attribute, $this->options);
    }
}