<?php

namespace codetitan\gateway;

use Yii;
use yii\helpers\Json;

/**

 * @version 2.0.0
 */
class Paypal extends BasePayment
{
    public $baseUri;
    public $clientId;
    public $secret;

    private $accessToken;

    function __construct($baseUri, $clientId, $secret)
    {
        $this->baseUri = $baseUri;
        $this->clientId = $clientId;
        $this->secret = $secret;
    }

    public function make($urls)
    {
        $this->wrap();

        $postData = [
            'intent' => 'sale',
            'redirect_urls' => [
                'return_url' => $urls['return'],
                'cancel_url' => $urls['cancel'],
            ],
            'payer' => [
                'payment_method' => 'paypal',
            ],
            'transactions' => $this->transactions,
        ];
    
        $url = $this->baseUri.'/v1/payments/payment';
        return $this->performPostCall($url, Json::encode($postData));
    }

    public function execute($paymentId, $payerId)
    {
        $postData = ['payer_id' => $payerId];

        $url = $this->baseUri.'/v1/payments/payment/'.$paymentId.'/execute';
        return $this->performPostCall($url, Json::encode($postData));
    }

    private function getAccessToken()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->baseUri.'/v1/oauth2/token');
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_USERPWD, $this->clientId.':'.$this->secret);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'grant_type=client_credentials');
        $result = curl_exec($ch);

        if (empty($result)) die(curl_error($ch));
        else {
            $json = Json::decode($result);
            $this->accessToken = $json['access_token'];
        }

        curl_close($ch);
    }

    private function performPostCall($url, $postData) 
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: Bearer '.$this->getAccessToken(),
            'Accept: application/json',
            'Content-Type: application/json'
        ]);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData); 
        $result = curl_exec($ch);

        if (empty($result)) die(curl_error($ch));
        else return json::decode($result);
    
        curl_close($ch);
    }
}