<?php

namespace codetitan\handlers;

use Yii;

/**
 * @version 2.0.1
 *
 * Handles action from /widgets/ActionBar
 * Includes predefined attributes
 * Support single or multiple primary keys
 */
class ActionHandler
{
    public $primaryKey;
    public $route;
    public $action;
    public $hasDelete = false;

    public $model;
    public $selections = [];
    public $actions = [];

    public $triggerEvent = true;

    public function registerTranslations()
    {
        $i18n = Yii::$app->i18n;
        $i18n->translations['widgets/actionbar'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@vendor/codetitan/widgets/ActionBar/messages',
            'fileMap' => [
                'widgets/actionbar' => 'actionbar.php',
            ],
        ];
    }

    public static function t($message, $params = [], $language = null)
    {
        return Yii::t('widgets/actionbar', $message, $params, $language);
    }

    /**
     * Populate default actions
     */
    public function populate()
    {
        $action['new'] = function () {
            return $this->redirect(['/'.$this->route.'/create']);
        };

        $action['edit'] = function () {
            $this->editItem();
        };

        $action['cancel'] = function () {
            return $this->redirect(['/'.$this->route]);
        };
        $action['close'] = $action['cancel'];
        $action['back'] = $action['cancel'];

        $action['delete'] = function () {
            $count = $this->deleteItem();
            if ($count) $this->setFlash('success', static::t('{count} item deleted', ['count' => $count]).'.');
            if ($this->model->{$this->primaryKey[0]}) return $this->redirect(['/'.$this->route]);
        };

        $this->actions = $action;
    }

    function __construct($model, $actions = [])
    {
        $this->registerTranslations();

        $this->populate();
        if ($actions) $this->setAction($actions);

        $this->model = $model;
        $this->primaryKey = $model->tableSchema->primaryKey;
        if (!$this->route) $this->route = dirname(Yii::$app->controller->route);
        if ($model->hasAttribute('is_deleted')) $this->hasDelete = true;

        $this->action = key(Yii::$app->request->post('action'));
        $this->selections = \yii\helpers\Json::decode(Yii::$app->request->post('actionbar_selection'));

        // For single record via model id, insert into selections
        if (!$this->selections && isset($this->model->{$this->primaryKey[0]}) && $this->model->{$this->primaryKey[0]}) {
            if (count($this->primaryKey) > 1) {
                $selection = [];
                foreach ($this->primaryKey as $pk) {
                    $selection += [$pk => $this->model->{$pk}];
                }
                $this->selections = [$selection];
            } else {
                $this->selections = [$this->model->{$this->primaryKey[0]}];
            }
        }
    }

    public function setAction($actions)
    {
        $this->actions = array_merge($this->actions, $actions);
        return $this;
    }

    public function execute()
    {
        if (array_key_exists($this->action, $this->actions)) {
            return call_user_func($this->actions[$this->action], $this->model, $this->selections, $this);
        }

        return $this->action;
    }

    /**
     * Perform built in actions
     */
    protected function editItem()
    {
        if ($this->selections) {
            if (count($this->primaryKey) > 1) {
                $params = [];
                foreach ($this->selections[0] as $pk => $val) {
                    $params[] = $pk.'='.$val;
                }

                $url = \yii\helpers\Url::to(['/'.$this->route.'/update']).'?'.implode('&', $params);
                return $this->redirect($url);
            } else {
                // If exist only ONE primary key, use id instead of $this->primaryKey[0]
                return $this->redirect(['/'.$this->route.'/update', 'id' => $this->selections[0]]);
            }
        }
    }

    protected function deleteItem()
    {
        if ($this->hasDelete) $count = $this->updateAll(['is_deleted' => 1]);
        else $count = $this->deleteAll();
        return $count;
    }

    protected function publishItem()
    {
        $attr = [];
        if ($this->model->hasAttribute('published_at')) {
            $attr['published_at'] = date('U');
        }

        if ($this->model->hasAttribute('published_by') && !Yii::$app->user->isGuest) {
            $attr['published_by'] = Yii::$app->user->id;
        }

        $count = $this->updateAll(array_merge($attr, ['is_published' => 1]));

        return $count;
    }

    protected function unpublishItem()
    {
        $count = $this->updateAll(['is_published' => 0]);
        return $count;
    }

    /**
     * Getter, setter and utility tools
     */
    public function updateAll($attributes, $condition = [])
    {
        $count = 0;
        $model = $this->model;
        if (count($this->primaryKey) > 1) {
            foreach ($this->selections as $selection) {
                $selection += $condition;
                if ($this->triggerEvent) {
                    $items = $model::findAll($this->makeCondition($selection));
                    foreach ($items as $item) {
                        $item->setAttributes($attributes, false);
                        if ($item->save(false)) $count++;
                    }
                } else {
                    $count += $model::updateAll($attributes, $this->makeCondition($selection));
                }
            }
        } else {
            if ($this->triggerEvent) {
                $items = $model::findAll($this->makeCondition($condition));
                foreach ($items as $item) {
                    $item->setAttributes($attributes, false);
                    if ($item->save(false)) $count++;
                }
            } else {
                $count = $model::updateAll($attributes, $this->makeCondition($condition));
            }
        }

        return $count;
    }

    public function deleteAll($condition = [])
    {
        $count = 0;
        $model = $this->model;
        if (count($this->primaryKey) > 1) {
            foreach ($this->selections as $selection) {
                $selection += $condition;
                if ($this->triggerEvent) {
                    $items = $model::findAll($this->makeCondition($selection));
                    foreach ($items as $item) {
                        if ($item->delete() !== false) $count++;
                    }
                } else {
                    $count += $model::deleteAll($this->makeCondition($selection));
                }
            }
        } else {
            if ($this->triggerEvent) {
                $items = $model::findAll($this->makeCondition($condition));
                foreach ($items as $item) {
                    if ($item->delete() !== false) $count++;
                }
            } else {
                $count = $model::deleteAll($this->makeCondition($condition));
            }
        }

        return $count;
    }

    public function setFlash($type = 'success', $message)
    {
        if ($type == 'success') $status = '<b>'.static::t('Success').':</b>';
        Yii::$app->getSession()->setFlash($type, $status.' '.$message);
    }

    public function redirect($url) 
    {
        Yii::$app->getResponse()->redirect($url);
        Yii::$app->end();
    }

    protected function makeCondition($criteria = [])
    {
        $condition = [];
        if (count($this->primaryKey) == 1) {
            $condition += [$this->primaryKey[0] => $this->selections];
        }

        $condition += $criteria;
        if ($this->hasDelete && !isset($params['is_deleted'])) {
            $condition += ['is_deleted' => 0];
        }

        return $condition;
    }
}