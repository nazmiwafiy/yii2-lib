/**
 * @version 2.0.0
 */
function initOption(element, callback, url) {
    var id = $(element).val();
    if (id !== '') {
        url += (url.indexOf('?') >= 0)?'&':'?';
        $.ajax(url + 'id=' + id, {
            dataType: 'json'
        }).done(function(data) {
            callback(data.results);
        });
    }
}

// Multi select version
function initOptions(element, callback, url) {
    var ids = $(element).val();
    if (ids !== '') {
        url += (url.indexOf('?') >= 0)?'&':'?';
        $.ajax(url + 'ids=' + ids, {
            dataType: 'json'
        }).done(function(data) {
            callback(data.results);
        });
    }
}

function formatCountry(obj) {
    if (obj.id) return '<span class="flag-icon flag-icon-'+ obj.id.toLowerCase() + '"></span> ' + obj.text;
    else return obj.text;
}