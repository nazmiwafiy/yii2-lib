<?php

namespace codetitan\widgets;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;

/**
 * @copyright Copyright (c) 2016 Code Titan
 * @author David Cheang <david.cheang@codetitan.com.my>
 * @version 1.0.0
 */
class LookupInput extends \yii\widgets\InputWidget
{
    public $url;
    public $initValueText = null;
    public $height = '300px';

    /**
     * @inheritdoc
     */
    public function run()
    {
        parent::run();

        return $this->renderWidget();
    }

    /**
     * Renders the widget.
     */
    private function renderWidget()
    {
        $name = isset($options['name']) ? $options['name'] : Html::getInputName($this->model, $this->attribute);
        $value = isset($options['value']) ? $options['value'] : Html::getAttributeValue($this->model, $this->attribute);

        $options = $this->options;
        if (!array_key_exists('id', $this->options)) {
            $options['id'] = Html::getInputId($this->model, $this->attribute);
        }

        $this->renderModal($options['id']);
        return $this->lookupInput($name, $value, $options);
    }

    /**
     * Renders the lookup input.
     */
    private function lookupInput($name, $value = null, $options = [])
    {
        $selection_id = $options['id'].'-selection';
        if (!isset($options['disabled'])) $options['disabled'] = false;
        if (!$options['disabled']) {
            Yii::$app->view->registerCss("
                #".$selection_id." {
                    background-color:#fff;
                    cursor:default;
                }
            ");

            Yii::$app->view->registerJs("
                $('#".$selection_id."').on('click', function() {
                    openLookup($(this).attr('id').replace('-selection', ''));
                });

                $('#".$selection_id."').on('focus', function(e) { 
                    e.preventDefault();
                    $(this).blur();
                });
            ", \yii\web\View::POS_END);
        }

        $btn['lookup'] = Html::button('<i class="glyphicon glyphicon-search"></i>', [
            'class' => 'btn btn-primary',
            'onclick' => "openLookup('".$options['id']."')",
            'disabled' => $options['disabled'],
        ]);

        $btn['remove'] = Html::button('<i class="glyphicon glyphicon-remove"></i>', [
            'class' => 'btn btn-default',
            'onclick' => "clearLookup('".$options['id']."')",
            'disabled' => $options['disabled'],
        ]);

        $html = Html::input('hidden', $name, $value, ['id' => $options['id']]);
        if (!$value) $this->initValueText = null;

        $options['id'] = $selection_id;
        $options = array_merge(['class' => 'form-control'], $options);
        $input = Html::input('text', 'lookup-'.$name, $this->initValueText, $options);
        $input .= Html::tag('span', $btn['remove'].$btn['lookup'], ['class' => 'input-group-btn']);

        $html .= Html::tag('div', $input, ['class' => 'input-group']);
        return $html;
    }

    /**
     * Renders the lookup modal.
     */
    private function renderModal($id)
    {
        Yii::$app->view->registerJs("
            function openLookup(id) {
                $('#lookup_modal-'+id).modal('show');
            }

            function clearLookup(id) {
                $('#'+id).val('');
                $('#'+id+'-selection').val('');
            }

            function selectLookup(id, val, text) {
                $('#'+id).val(val);
                $('#'+id+'-selection').val(text);
                $('#lookup_modal-'+id).modal('hide');
            }
        ", \yii\web\View::POS_END, 'lookup-input');

        Modal::begin([
            'id' => 'lookup_modal-'.$id,
            'header' => '<b style="font-size:16px;">Lookup</b>',
        ]);

        echo '<iframe src="'.Url::to($this->url).'" style="border:none;width:100%;height:'.$this->height.';"></iframe>';

        Modal::end();
    }
}