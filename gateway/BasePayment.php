<?php

namespace codetitan\gateway;

/**
 * @version 2.0.0
 */
class BasePayment
{
    public static $attributeOrder = [
        'currency',
        'quantity',
        'price',
        'name',
    ];

    public static $defaultAttribute = [
        'currency' => 'USD',
        'quantity' => 1,
        'price' => 1,
    ];

    private $items = [];
    public $transactions = [];

    /**
     * Add transaction item.
     */
    public function add($options = [])
    {
        $this->items[] = array_merge(static::$defaultAttribute, static::loadAttributes($options));
    }

    /**
     * Wrap items into transaction record.
     */
    public function wrap($currency = 'USD')
    {
        $total = 0;
        foreach ($this->items as $item) {
            $total += ($item['quantity'] * $item['price']);
        }

        $this->transactions[] = [
            'amount' => [
                'total' => sprintf('%.2f', $total),
                'currency' => $currency,
            ],
            'item_list' => [
                'items' => $this->items,
            ],
        ];
    }

    /**
     * Load attributes to array.
     */
    private static function loadAttributes($attributes)
    {
        if (count($attributes) > 1) {
            $sorted = [];
            foreach (static::$attributeOrder as $name) {
                if (isset($attributes[$name])) {
                    $sorted[$name] = $attributes[$name];
                }
            }
            $attributes = array_merge($sorted, $attributes);
        }

        $output = [];
        foreach ($attributes as $name => $value) {
            $output[$name] = $value;
        }

        return $output;
    }
}