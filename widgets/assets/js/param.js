/**
 * @copyright Copyright (c) 2016 Code Titan
 * @author David Cheang <david.cheang@codetitan.com.my>
 * @version 2.0.0
 */
function updateGetParam(uri, key, value) {
	uri = decodeURI(uri);

	var regex = new RegExp('([?&])' + escapeParamKey(key) + '=.*?(&|$)', 'i');
	var separator = uri.indexOf('?') !== -1 ? '&' : '?';

	if (uri.match(regex)) {
		return uri.replace(regex, '$1' + key + '=' + value + '$2');
	} else {
		return uri + separator + key + '=' + value;
	}
}

function fetchGetParam(uri, key) {
    var regex = new RegExp('[?&]' + escapeParamKey(key) + '=([^&#]*)', 'i');
	results = regex.exec(decodeURI(uri));
	
	return (results === null) ? '' : results[1].replace(/\+/g, ' ');
}

function removeGetParam(uri, key) {
    var regex = new RegExp('[?&]' + escapeParamKey(key) + '=([^&#]*)', 'i');
	uri = decodeURI(uri);

	return uri.replace(regex, '');
}

function escapeParamKey(key) {
	return key.replace('[', '\\[').replace(']', '\\]');
}