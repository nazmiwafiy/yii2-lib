<?php

namespace codetitan\mobile;

use Yii;

/**
 * @version 1.0.0
 */
class AppleMessenger
{
    public $url;
    public $certificate;
    public $passphrase;
    private $fp = null;

    public function __construct($url, $certificate, $passphrase)
    {
        $this->url = $url;
        $this->certificate = $certificate;
        $this->passphrase = $passphrase;
    }

    /**
     * Connect to APNS
     */
    public function connect()
    {
        $context = stream_context_create();
        stream_context_set_option($context, 'ssl', 'local_cert', $this->certificate);
        stream_context_set_option($context, 'ssl', 'passphrase', $this->passphrase);

        $this->fp = stream_socket_client(
            'ssl://'.$this->url, $errno, $errstr, 60, 
            STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, 
            $context
        );

        // Failed to connect to APNS
        if (!$this->fp) {
            return false;
        }

        stream_set_blocking($this->fp, 0);

        return true;
    }

    /**
     * Disconnect from APNS
     */
    public function disconnect()
    {
        fclose($this->fp);
        $this->fp = null;
    }

    /**
     * Send notification
     */
    public function send($deviceToken, $payload, $uniqueId = null)
    {
        if (!$uniqueId) $uniqueId = uniqid();

        if (strlen($deviceToken) != 64) {
            return false;
        }

        if (strlen($payload) < 10) {
            return false;
        }

        $inner = chr(1)
            . pack('n', 32)                 // token length (2 bytes)
            . pack('H*', $deviceToken)      // device token (32 bytes)

            . chr(2)
            . pack('n', strlen($payload))   // payload length (2 bytes)
            . $payload

            . chr(3)
            . pack('n', 4)
            . pack('N', $uniqueId)

            . chr(4)
            . pack('n', 4)
            . pack('N', time() + 86400)

            . chr(5)
            . pack('n', 1)
            . chr(10);

        $notification = chr(2)
            . pack('N', strlen($inner))
            . $inner;

        $result = fwrite($this->fp, $notification, strlen($notification));
        if ($result) return true;

        return false;
    }

    /**
     * Generate payload (JSON encoded)
     */
    public function generatePayload($message, $options = [])
    {
        $message = mb_strimwidth($message, 0, 220, '..');

        $payload['aps'] = [
            'alert' => $message,
            'sound' => 'default',
            'badge' => 1,
            'content-available' => 1,
        ];

        if ($options) {
            $payload = array_merge($payload, $options);
        }

        return json_encode($payload);
    }
}