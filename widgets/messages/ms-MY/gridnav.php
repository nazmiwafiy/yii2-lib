<?php

return [
    'Page' => 'Halaman',
    'of {total} pages' => 'dari {total} jumlah halaman',
    'Total <b>{total}</b> records' => 'Jumlah <b>{total}</b> rekod',
];