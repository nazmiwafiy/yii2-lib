<?php

namespace codetitan\helpers;

use Yii;

/**
 * @version 1.0.0
 */
class RbacHelper
{
    /**
     * Get roles by user
     */
    public static function getRoles($userId)
    {
        $roles = \yii\helpers\ArrayHelper::getColumn(Yii::$app->authManager->getRolesByUser($userId), 'name');
        return $roles;
    }

    /**
     * Determine if user has roles
     */
    public static function hasRoles($userId, $criterias, $operator = 'AND')
    {
        $matches = 0;
        $roles = static::getRoles($userId);
        foreach ($criterias as $criteria) {
            if (in_array($criteria, $roles)) $matches = 1;
        }

        if ($operator == 'OR') {
            if ($matches >= 1) return true;
        } else {
            if ($matches == count($criterias)) return true;
        }

        return false;
    }

    /**
     * Assign roles to user
     */
    public static function setRoles($userId, $roles)
    {
        static::revokeAllRoles($userId);
        foreach ($roles as $role) {
            if ($selectedRole = Yii::$app->authManager->getRole($role)) {
                Yii::$app->authManager->assign($selectedRole, $userId);
            }
        }
    }

    /**
     * Revoke all user roles
     */
    public static function revokeAllRoles($userId)
    {
        Yii::$app->authManager->revokeAll($userId);
    }
}