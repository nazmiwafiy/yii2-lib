<?php

namespace codetitan\tree;

use Yii;
use yii\db\Query;

/**
 * @version 2.0.0
 *
 * Handles closure tree manipulation (Hierarchical data)
 */
class BaseClosure
{
    public $table;

    /**
     * Initialize table variables
     */
    function __construct($reference, $closure = 'closure') 
    {
        $this->table = ['reference' => $reference, 'closure' => $closure];
    }

    /**
     * Requires insertId from insert operation from cross-reference table
     */
    public function insertChild($node, $insertId)
    {
        if (is_int($insertId)) {
            $sql = 'INSERT INTO {{%'.$this->table['closure'].'}} (ancestor, descendant, depth) 
                    SELECT ancestor, '.$insertId.', depth+1 FROM {{%'.$this->table['closure'].'}} 
                    WHERE descendant='.$node.' UNION ALL SELECT '.$insertId.', '.$insertId.', 0';
            $connection = Yii::$app->db;
            $connection->createCommand($sql)->execute();
        }
    }

    /**
     * Delete the node and all nodes under it
     */
    public function deleteChild($node)
    {
        $query = new Query;
        $query->select('descendant')->from('{{%'.$this->table['closure'].'}}')->where(['ancestor' => $node]);
        $result = $query->createCommand()->queryAll();

        $connection = Yii::$app->db;
        $connection->createCommand()->delete('{{%'.$this->table['closure'].'}}', ['descendant' => $result])->execute();
    }

    /**
     * Move the node and all nodes under it
     */
    public function moveChild($node, $parentNode) 
    {
        // Disconnect subtree from parent
        $sql = 'DELETE a FROM {{%'.$this->table['closure'].'}} AS a
                JOIN {{%'.$this->table['closure'].'}} AS d ON a.descendant = d.descendant
                LEFT JOIN {{%'.$this->table['closure'].'}} AS x
                ON x.ancestor = d.ancestor AND x.descendant = a.ancestor
                WHERE d.ancestor = '.$node.' AND x.ancestor IS NULL';
        $connection = Yii::$app->db;
        $connection->createCommand($sql)->execute();

        // Connect subtree to the new parent
        $sql = 'INSERT INTO {{%'.$this->table['closure'].'}} (ancestor, descendant, depth)
                SELECT supertree.ancestor, subtree.descendant, supertree.depth + subtree.depth + 1
                FROM {{%'.$this->table['closure'].'}} AS supertree JOIN {{%'.$this->table['closure'].'}} AS subtree
                WHERE subtree.ancestor = '.$node.' AND supertree.descendant = '.$parentNode;
        $connection = Yii::$app->db;
        $connection->createCommand($sql)->execute();
    }

    /**
     * Query ancestor nodes
     * @return array
     */
    public function queryAncestor($node, $depth = null, $params = [])
    {
        $query = new Query;
        $query->select('n.*')->from('{{%'.$this->table['reference'].'}} AS n')
            ->innerJoin('{{%'.$this->table['closure'].'}} AS t', 'n.id = t.ancestor')
            ->where('t.descendant = '.$node);
        if ($depth) $query->andWhere('t.depth = '.$depth);
        if (isset($params['where'])) $query->andWhere($params['where']);
        if (isset($params['orderBy'])) $query->orderBy($params['orderBy']);
        if (isset($params['limit'])) $query->limit($params['limit']);

        return $query->createCommand()->queryAll();
    }

    /**
     * Query descendant nodes
     * @return array
     */
    public function queryDescendant($node, $depth = null, $params = [])
    {
        $query = new Query;
        $query->select('n.*')->from('{{%'.$this->table['reference'].'}} AS n')
            ->innerJoin('{{%'.$this->table['closure'].'}} AS t', 'n.id = t.descendant')
            ->where('t.ancestor = '.$node);
        if ($depth) $query->andWhere('t.depth = '.$depth);
        if (isset($params['where'])) $query->andWhere($params['where']);
        if (isset($params['orderBy'])) $query->orderBy($params['orderBy']);
        if (isset($params['limit'])) $query->limit($params['limit']);

        return $query->createCommand()->queryAll();
    }

    /**
     * Query depth of a particular node
     * @return int
     */
    public function getDepth($node, $params = [])
    {
        $query = new Query;
        $query->select('MAX(depth)')->from('{{%'.$this->table['closure'].'}}')->where(['descendant' => $node]);
        if (isset($params['where'])) $query->andWhere($params['where']);
        $depth = $query->createCommand()->queryScalar();

        if ($depth < 0) $depth = 0;

        return $depth;
    }
}