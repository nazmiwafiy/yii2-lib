<?php

namespace codetitan\handlers;

use Yii;

/**
 * @version 1.0.0
 */
class OptionHandler {

    public static function populate($params = []) {
        $data['yes-no'] = [1 => Yii::t('app', 'Yes'), 0 => Yii::t('app', 'No')];

        // Using ISO-8601 standard
        $data['weekday'] = [
            1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday', 7 => 'Sunday'
        ];

        $data['weekday-short'] = [1 => 'Mon', 2 => 'Tue', 3 => 'Wed', 4 => 'Thu', 5 => 'Fri', 6 => 'Sat', 7 => 'Sun'];

        $data['date-month'] = [
            '01' => 'January', '02' => 'February', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June',
            '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December'
        ];

        $data['date-month-short'] = [
            '01' => 'Jan', '02' => 'Feb', '03' => 'Mar', '04' => 'Apr', '05' => 'May', '06' => 'Jun',
            '07' => 'Jul', '08' => 'Aug', '09' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec'
        ];

        $data['date-day'] = [
            'options' => function() {
                $item = [];
                for ($i = 1; $i <= 31; $i++) {
                    $item[str_pad($i, 2, 0, STR_PAD_LEFT)] = $i;
                }

                return $item;
            },
        ];

        $data['date-year'] = [
            'options' => function() use ($params) {
                $item = [];
                $start = (isset($params['start'])) ? $params['start'] : date('Y');
                $end = (isset($params['end'])) ? $params['end'] : date('Y') - 70;

                if ($start > $end) {
                    for ($i = $start; $i >= $end; $i--) {
                        $item[$i] = $i;
                    }
                } else {
                    for ($i = $start; $i <= $end; $i++) {
                        $item[$i] = $i;
                    }
                }

                return $item;
            },
        ];

        $data['numeric-range'] = [
            'options' => function() use ($params) {
                $start = (isset($params['start'])) ? $params['start'] : 1;
                $end = (isset($params['end'])) ? $params['end'] : 99;

                if (isset($params['step']))
                    $item = range($start, $end, $params['step']);
                else
                    $item = range($start, $end);

                return $item;
            },
            'mirror' => true,
        ];

        return $data;
    }

    public static function render($template, $params = []) {
        $options = [];
        $data = static::populate($params);

        if (isset($data[$template])) {
            if (isset($data[$template]['options'])) {
                if (is_array($data[$template]['options']))
                    $options = $data[$template]['options'];
                else
                    $options = call_user_func($data[$template]['options']);

                if (isset($data[$template]['mirror']) && $data[$template]['mirror']) {
                    $options = static::mirror($options);
                }

                if (isset($data[$template]['shift']) && $data[$template]['shift']) {
                    $options = static::shift($options);
                }
            } else {
                if (is_array($data[$template]))
                    $options = $data[$template];
            }
        }

        return $options;
    }

    public static function resolve($template, $val, $params = []) {
        $options = static::render($template, $params);

        if (isset($options[$val]))
            return $options[$val];
        else
            return null;
    }

    private static function mirror($options) {
        $options = array_combine($options, $options);
        return $options;
    }

    private static function shift($options) {
        array_unshift($options, 'dummy');
        unset($options[0]);
        return $options;
    }

}
