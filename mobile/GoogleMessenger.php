<?php

namespace codetitan\mobile;

use Yii;

/**
 * @version 1.0.0
 */
class GoogleMessenger
{
    public $url;
    public $serverKey;

    public function __construct($url, $serverKey)
    {
        $this->url = $url;
        $this->serverKey = $serverKey;
    }

    /**
     * Send notification
     */
    public function send($deviceToken, $payload, $uniqueId = null)
    {
        if (!$uniqueId) $uniqueId = uniqid();

        if (strlen($deviceToken) < 64) {
            return false;
        }

        $data = json_decode($payload);
        $data->to = $deviceToken;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'Authorization: key='.$this->serverKey]);
        curl_setopt($ch, CURLOPT_POST, count($data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        if ($httpCode == 200) return true;

        return false;
    }

    /**
     * Generate payload (JSON encoded)
     */
    public function generatePayload($message, $options = [])
    {
        $message = mb_strimwidth($message, 0, 220, '..');

        $payload['notification'] = [
            'title' => 'Notification',
            'body' => $message,
            'sound' => 'default',
        ];

        if ($options) {
            $payload = array_merge($payload, $options);
        }

        return json_encode($payload);
    }
}